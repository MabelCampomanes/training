import React from 'react';
import './App.css';
import TestComponent from './components/test';

function App() {
  return (
    <div className="App">
      <TestComponent></TestComponent>
    </div>
  );
}

export default App;
