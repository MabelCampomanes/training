import React, { useState, useEffect } from 'react';

const TestComponent = () => {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/users');
      const data = await response.json();
      setUsers(data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <h1>Lista de emails</h1>
      <ul>
        {users.map((user) => (
          <li key={user.id}>
            {user.email}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default TestComponent;